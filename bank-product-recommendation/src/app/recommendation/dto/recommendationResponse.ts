export interface RecommendationResponse {
  recommendedProducts: string[];
}
