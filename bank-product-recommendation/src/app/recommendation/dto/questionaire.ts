export interface Questionnaire {
  age: number;
  student: boolean;
  income: number;
}
