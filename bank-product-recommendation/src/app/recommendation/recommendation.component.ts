import { Component } from '@angular/core';
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {RecommendationService} from "./service/recommendation.service";
import {RecommendationResponse} from "./dto/recommendationResponse";
import {CommonModule} from "@angular/common";
import {HttpClientModule} from "@angular/common/http";

@Component({
  selector: 'app-recommendation',
  standalone: true,
  imports: [
    ReactiveFormsModule, CommonModule, HttpClientModule
  ],
  providers: [RecommendationService],
  templateUrl: './recommendation.component.html',
  styleUrl: './recommendation.component.scss'
})
export class RecommendationComponent {
  recommendationForm: FormGroup;
  recommendations: string[] = [];

  constructor(
    private fb: FormBuilder,
    private recommendationService: RecommendationService) {
    this.recommendationForm = this.fb.group({
      age: ['', Validators.required],
      student: [false, Validators.required],
      income: ['', Validators.required]
    });
  }

  onSubmit() {
    if (this.recommendationForm.valid) {
      this.recommendationService.getRecommendations(this.recommendationForm.value)
        .subscribe((response: RecommendationResponse) => {
          this.recommendations = response.recommendedProducts;
        });
    }
  }
}
