import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Questionnaire} from "../dto/questionaire";
import {Observable} from "rxjs";
import {RecommendationResponse} from "../dto/recommendationResponse";

@Injectable({
  providedIn: 'root'
})
export class RecommendationService {
  private apiUrl = 'http://localhost:8080/api/recommendations';

  constructor(private http: HttpClient) {
  }

  getRecommendations(questionnaire: Questionnaire): Observable<RecommendationResponse> {
    return this.http.post<RecommendationResponse>(this.apiUrl, questionnaire);
  }
}
