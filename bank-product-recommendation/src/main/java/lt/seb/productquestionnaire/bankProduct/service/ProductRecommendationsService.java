package lt.seb.productquestionnaire.bankProduct.service;

import lt.seb.productquestionnaire.bankProduct.model.ProductRecommendation;
import lt.seb.productquestionnaire.bankProduct.model.Questionnaire;

import java.util.ArrayList;
import java.util.List;

public class ProductRecommendationsService {

  public ProductRecommendation getRecommendations(Questionnaire questionnaire) {
    List<String> products = new ArrayList<>();

    int age = questionnaire.getAge();
    boolean student = questionnaire.isStudent();
    double income = questionnaire.getIncome();

    if (income > 0 && age > 17) {
      products.add("Current Account");
    }
    if (income > 40000 && age > 17) {
      products.add("Current Account Plus");
    }
    if (age < 18) {
      products.add("Junior Saver Account");
    }
    if (student && age > 17) {
      products.add("Student Account");
    }
    if (age >= 65) {
      products.add("Senior Account");
    }
    if (income < 12001 && age > 17) {
      products.add("Debit Card");
    }
    if (income > 12000 && age > 17) {
      products.add("Credit Card");
    }
    if (income > 40000 && age > 17) {
      products.add("Gold Credit Card");
    }

    return new ProductRecommendation(products);
  }
}
