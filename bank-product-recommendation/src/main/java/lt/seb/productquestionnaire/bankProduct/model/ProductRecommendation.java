package lt.seb.productquestionnaire.bankProduct.model;

import java.util.List;

public class ProductRecommendation {

  private List<String> recommendedProducts;

  public ProductRecommendation(List<String> products) {
  }

  public List<String> getRecommendedProducts() {
    return recommendedProducts;
  }

  public void setRecommendedProducts(List<String> recommendedProducts) {
    this.recommendedProducts = recommendedProducts;
  }
}
