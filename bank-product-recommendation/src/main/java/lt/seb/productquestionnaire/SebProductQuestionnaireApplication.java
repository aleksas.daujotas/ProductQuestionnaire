package lt.seb.productquestionnaire

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SebProductQuestionnaireApplication {

  public static void main(String[] args) {
    SpringApplication.run(SebProductQuestionnaireApplication.class, args);
  }
}
