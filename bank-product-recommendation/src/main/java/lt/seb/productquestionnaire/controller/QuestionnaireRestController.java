package lt.seb.productquestionnaire.controller;

import lt.seb.productquestionnaire.bankProduct.model.ProductRecommendation;
import lt.seb.productquestionnaire.bankProduct.model.Questionnaire;
import lt.seb.productquestionnaire.bankProduct.service.ProductRecommendationsService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/recommendations")
public class QuestionnaireRestController {
  private ProductRecommendationsService recommendationsService;

  @PostMapping
  public ProductRecommendation getRecommendations(@RequestBody Questionnaire questionnaire) {
    return recommendationsService.getRecommendations(questionnaire);
  }

  public void setRecommendationsService(ProductRecommendationsService recommendationsService) {
    this.recommendationsService = recommendationsService;
  }
}
